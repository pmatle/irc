/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_command.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/23 08:18:50 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:02:19 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

char	*full_data(char *path)
{
	char	*str;

	str = ft_strjoin(path, "\n");
	if (!str)
		return (NULL);
	return (str);
}

void	handle_who(int fd, int channel)
{
	char		*list;
	t_listnode	**node;

	list = get_members(channel, fd);
	if (!list)
		send_data(fd, "Error: Something went wrong...try again\n", 40);
	if (ft_strlen(list) > 14)
		send_data(fd, list, ft_strlen(list));
	else
		send_data(fd, "You are the only member\n", 24);
	free(list);
}

void	execute_commands(int fd, char **arr, int channel, int flag)
{
	if (flag == 1)
		handle_nick(fd, arr);
	if (flag == 2)
		handle_who(fd, channel);
	if (flag == 3)
		handle_msg(fd, channel, arr);
	if (flag == 4)
		handle_join(fd, channel, arr);
	if (flag == 5)
		handle_list(fd, channel);
	if (flag == 6)
		handle_create(fd, channel, arr);
	if (flag == 7)
		handle_enter(fd, arr);
	if (flag == 8)
		send_data(fd, "Already connected to server\n", 28);
	if (flag == 0)
		send_data(fd, "IRC: Command not found\n", 23);
}

int		handle_commands(char *str, int fd, t_listnode *members, int channel)
{
	int			ret;
	char		**arr;

	arr = ft_strsplit(str, ' ');
	if (!arr)
		return (0);
	ret = check_command(arr[0]);
	execute_commands(fd, arr, channel, ret);
	free_2d(arr);
	(void)members;
	return (1);
}

int		handle_client(int fd, char *str)
{
	char	**arr;
	int		err;

	arr = ft_strsplit(str, ' ');
	if (arr)
	{
		err = handle_server_command(fd, arr, str);
		if (err)
		{
			free_2d(arr);
			return (0);
		}
		free_2d(arr);
		return (1);
	}
	return (0);
}
