/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/19 10:30:36 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 09:52:44 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	handle_enter(int fd, char **arr)
{
	int		x;
	t_list	list;

	x = 0;
	while (x < g_info.graph->vertexes)
	{
		list = g_info.graph->list[x];
		if (ft_strcmp(list.channel_name, arr[1]) == 0)
		{
			if (FD_ISSET(fd, &list.channel_set))
			{
				enter_channel(fd, x);
			}
			else
			{
				send_data(fd, "Error: You are not a channel member\n", 36);
			}
			break ;
		}
		x++;
	}
}
