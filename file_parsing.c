/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_parsing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/23 13:11:15 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/29 06:45:08 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		get_file_ptr(char *str, t_file *file)
{
	int				fd;
	struct stat		buff;

	if ((fd = open(str, O_RDONLY)) < 0)
		return (0);
	if (fstat(fd, &buff) < 0)
		return (0);
	file->data = mmap(0, buff.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (file->data == MAP_FAILED)
	{
		close(fd);
		return (0);
	}
	file->size = buff.st_size;
	return (1);
}
