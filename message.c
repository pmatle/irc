/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   message.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 14:35:31 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 14:34:33 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

char	*construct_message(int fd, char **arr)
{
	int			x;
	int			len;
	char		*res;
	char		*name;
	t_listnode	*sender;

	x = 0;
	sender = get_client(fd);
	len = ft_arr_strlen(arr) + ft_strlen(sender->name) + arr_len(arr) + 2;
	res = ft_strnew(len);
	name = ft_strdup(sender->name);
	if (!res || !name)
		return (NULL);
	len = arr_len(arr);
	res = memcpy(res, name, ft_strlen(name));
	res = ft_strcat(res, ": ");
	while (x < len)
	{
		res = ft_strcat(res, arr[x]);
		res = ft_strcat(res, " ");
		x++;
	}
	res = ft_strcat(res, "\n");
	free(name);
	return (res);
}

void	broadcast_message(int fd, int channel, char **arr)
{
	char			*tmp;
	char			*data;
	t_listnode		*user;

	user = g_info.graph->list[channel].users;
	while (user != NULL)
	{
		if (user->fd != fd)
		{
			data = construct_message(fd, &arr[2]);
			if (!data)
			{
				send_data(fd, "Error: Something went wrong...try again\n", 40);
				return ;
			}
			add_data(&user, data, 3);
		}
		user = user->next;
	}
}

void	private_message(int fd, int channel, char **arr)
{
	t_listnode		*user;
	char			*data;

	user = g_info.graph->list[channel].users;
	while (user != NULL)
	{
		if (ft_strcmp(user->name, arr[1]) == 0)
		{
			data = construct_message(fd, &arr[2]);
			if (!data)
			{
				send_data(fd, "Error: Something went wrong...try again\n", 40);
				return ;
			}
			add_data(&user, data, 3);
			break ;
		}
		user = user->next;
	}
}

void	send_message(int fd, int index)
{
	int				ret;
	t_listnode		*user;
	char			*message;

	user = g_info.graph->list[index].users;
	while (user != NULL)
	{
		if (user->fd == fd && user->message_count > 0)
		{
			message = retrieve_message(fd, index);
			if (!message)
				return ;
			ret = send(fd, message, ft_strlen(message), 0);
			if (ret != ft_strlen(message))
				add_data(&user, (message + ret), 3);
		}
		user = user->next;
	}
}

void	handle_writing(fd_set set, int max)
{
	int			fd;
	t_listnode	*client;

	fd = 0;
	while (fd <= max)
	{
		if (FD_ISSET(fd, &set))
		{
			client = get_client(fd);
			if (client)
				send_message(fd, client->curr_chan);
		}
		fd++;
	}
}
