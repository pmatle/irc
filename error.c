/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 09:04:11 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/07 10:29:28 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void		handle_error(int ecode, char *type)
{
	if (ft_strcmp(type, "client") == 0 && ecode == EAI_NONAME)
		ft_putendl_fd("client: Invalid ip address specified", 2);
	else
	{
		ft_putstr_fd(type, 2);
		ft_putstr_fd(": Something went wrong while starting the ", 2);
		ft_putendl_fd(type, 2);
	}
	exit(0);
}

void		cd_err_many(int fd, char *type)
{
	char	*str;

	str = ft_strjoin(type, ": too many arguments\n");
	if (str)
	{
		print_message(fd, str);
		free(str);
	}
	else
		print_message(fd, "An error occured...try again\n");
}

void		print_message(int fd, char *full)
{
	if (fd == 1)
		ft_putstr_fd(full, 2);
	else
		send_data(fd, full, ft_strlen(full));
}

void		print_cd_err(int fd, char *str, char *type)
{
	char	*full;
	char	*tmp;

	full = ft_strjoin(type, " : no such file or directory : ");
	tmp = ft_strjoin(full, str);
	free(full);
	full = tmp;
	tmp = ft_strjoin(full, "\n");
	free(full);
	full = tmp;
	if (full)
	{
		print_message(fd, full);
		free(full);
	}
	else
		print_message(fd, "An error occured...try again\n");
}
