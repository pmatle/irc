/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/21 14:28:40 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:12:26 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int				is_valid_port(char *str)
{
	int		port;

	port = ft_atoi(str);
	if (port > 0)
		return (1);
	return (0);
}

void			free_2d(char **arr)
{
	int		x;

	x = 0;
	while (arr[x])
	{
		free(arr[x]);
		x++;
	}
	free(arr[x]);
}

int				is_command(char *str)
{
	if (ft_strcmp(str, "/nick") == 0)
		return (1);
	else if (ft_strcmp(str, "/join") == 0)
		return (1);
	else if (ft_strcmp(str, "/who") == 0)
		return (1);
	else if (ft_strcmp(str, "/msg") == 0)
		return (1);
	else if (ft_strcmp(str, "/create") == 0)
		return (1);
	else if (ft_strcmp(str, "/invite") == 0)
		return (1);
	else if (ft_strcmp(str, "/enter") == 0)
		return (1);
	else if (ft_strcmp(str, "/delete") == 0)
		return (1);
	else if (ft_strcmp(str, "/connect") == 0)
		return (1);
	return (0);
}

struct addrinfo	*init(char *addr, char *port, char *type)
{
	struct addrinfo		*info;
	struct addrinfo		hints;
	int					err;

	ft_bzero(&hints, sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	err = getaddrinfo(addr, port, &hints, &info);
	if (err)
	{
		gai_strerror(err);
		handle_error(err, type);
	}
	return (info);
}

void			handle_server_cmds(int fd, char *str)
{
	int		len;
	char	*tmp;
	char	buff[255];

	ft_bzero(buff, 255);
	if (send_data(fd, str, ft_strlen(str)) && send_data(fd, "\n", 1))
	{
		while ((len = recv(fd, buff, 255, 0)) > 0)
		{
			buff[len] = '\0';
			if (len < 255)
			{
				ft_putstr(buff);
				break ;
			}
			ft_putstr(buff);
		}
	}
}
