/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_mem.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 08:56:36 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 07:39:08 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void		free_circular(t_buffer *buff)
{
	if (buff)
	{
		while (buff->front && buff->front->link != buff->front)
			remove_node(buff);
		free(buff);
	}
}

void		free_listnode(t_listnode *node)
{
	t_listnode	*tmp;

	while (node != NULL)
	{
		tmp = node;
		node = node->next;
		free_circular(tmp->buff);
		free_circular(tmp->messages);
	}
}
