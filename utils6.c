/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils6.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/22 07:50:23 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:16:04 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	enter_channel(int fd, int channel)
{
	int		x;
	t_list	list;

	x = 0;
	while (x < g_info.graph->vertexes)
	{
		list = g_info.graph->list[x];
		if (FD_ISSET(fd, &list.channel_set))
		{
			change_channel(fd, channel);
		}
		x++;
	}
}

void	get_set(int fd, fd_set *write, fd_set *read)
{
	FD_ZERO(write);
	FD_ZERO(read);
	FD_SET(0, read);
	FD_SET(1, write);
	FD_SET(fd, write);
	FD_SET(fd, read);
}

void	init_client(void)
{
	g_client.read_stdin_lines = 0;
	g_client.count = 0;
	g_client.read_stdin = init_buffer();
}
