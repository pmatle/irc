/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   channel.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/28 10:52:30 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 09:22:04 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		create_channel(t_listnode *node, char *channel)
{
	int			index;
	t_listnode	*copy;

	index = find_channel_slot();
	copy = new_node(node->name, node->fd);
	if (index != -1 && copy)
	{
		copy->curr_chan = index;
		ft_memcpy(g_info.graph->list[index].channel_name, channel,
				ft_strlen(channel));
		g_info.graph->list[index].free = 0;
		add_user(channel, copy);
		return (0);
	}
	return (-1);
}

char	*get_members(int index, int fd)
{
	char		*tmp;
	char		*tmp1;
	t_listnode	*user;
	char		*members_list;

	members_list = ft_strdup("\tMembers list\n");
	if (!members_list)
		return (NULL);
	user = g_info.graph->list[index].users;
	while (user != NULL)
	{
		if (user->fd != fd)
		{
			members_list = append_user(members_list, user->name);
			if (!members_list)
				return (NULL);
		}
		user = user->next;
	}
	return (members_list);
}

char	*append_channel(char *group, int fd, int index)
{
	fd_set			set;
	char			*tmp;
	t_listnode		*node;

	node = get_client(fd);
	tmp = ft_strjoin(group, g_info.graph->list[index].channel_name);
	free(group);
	set = g_info.graph->list[index].channel_set;
	if (FD_ISSET(fd, &set) && index == node->curr_chan)
		group = ft_strjoin(tmp, " (You are a member) *\n");
	else if (FD_ISSET(fd, &set))
		group = ft_strjoin(tmp, " (You are a member)\n");
	else
		group = ft_strjoin(tmp, "\n");
	free(tmp);
	return (group);
}

char	*get_channels(int fd, int channel)
{
	int			x;
	fd_set		set;
	char		*tmp;
	t_listnode	*node;
	char		*groups;

	x = 0;
	node = get_client(fd);
	groups = ft_strdup("\tChannel list :\n");
	if (!groups)
		return (NULL);
	while (x < g_info.graph->vertexes)
	{
		if (g_info.graph->list[x].free == 0)
			groups = append_channel(groups, fd, x);
		x++;
	}
	return (groups);
}
