/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 08:24:49 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 14:30:04 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		main(int argc, char **argv)
{
	if (argc == 1)
		handle_not_connected();
	else if (argc == 3)
	{
		if (is_valid_port(argv[2]))
			start_client(argv[1], argv[2]);
		else
			ft_putendl_fd("Error: Invalid port number", 2);
	}
	else
		ft_putendl_fd("./client [server] [port]", 2);
	return (1);
}
