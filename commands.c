/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 09:16:47 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 09:57:30 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	assign_name(int fd, char *name)
{
	int			x;
	t_listnode	*user;

	x = 0;
	while (x < g_info.graph->vertexes)
	{
		user = g_info.graph->list[0].users;
		while (user != NULL)
		{
			if (user->fd == fd)
			{
				ft_bzero(user->name, ft_strlen(user->name));
				ft_memcpy(user->name, name, ft_strlen(name));
				break ;
			}
			user = user->next;
		}
		x++;
	}
}

void	handle_nick(int fd, char **arr)
{
	int			x;
	t_listnode	*user;

	if (arr_len(arr) != 2)
	{
		send_data(fd, "Error: /nick takes in one argument\n", 35);
	}
	else if (ft_strlen(arr[1]) > 9)
	{
		send_data(fd, "Error: The name has to be less than eighth chars\n", 49);
	}
	else
	{
		assign_name(fd, arr[1]);
	}
}

void	handle_msg(int fd, int channel, char **arr)
{
	if (arr_len(arr) > 2)
	{
		if (ft_strcmp(arr[1], "@channel") == 0)
			broadcast_message(fd, channel, arr);
		else
			private_message(fd, channel, arr);
	}
	else
	{
		send_data(fd, "Usage: /msg <username/@channel> <message>\n", 42);
	}
}

void	handle_list(int fd, int channel)
{
	char	*list;

	list = get_channels(fd, channel);
	if (!list)
		send_data(fd, "Error: Something went wrong...try again\n", 40);
	else
		send_data(fd, list, ft_strlen(list));
}

void	handle_create(int fd, int channel, char **arr)
{
	int				err;
	t_listnode		*node;

	if (arr_len(arr) == 2)
	{
		node = get_client(fd);
		if (!node)
			send_data(fd, "Error: Something went wrong...try again\n", 40);
		err = create_channel(node, arr[1]);
		if (err)
			send_data(fd, "Error: Failed to create channel...try again\n", 44);
	}
	else
	{
		send_data(fd, "Usage: /create [channel]\n", 25);
	}
}
