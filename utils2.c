/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 08:37:30 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:16:12 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		check_command(char *str)
{
	if (ft_strcmp(str, "/nick") == 0)
		return (1);
	else if (ft_strcmp(str, "/who") == 0)
		return (2);
	else if (ft_strcmp(str, "/msg") == 0)
		return (3);
	else if (ft_strcmp(str, "/join") == 0)
		return (4);
	else if (ft_strcmp(str, "/list") == 0)
		return (5);
	else if (ft_strcmp(str, "/create") == 0)
		return (6);
	else if (ft_strcmp(str, "/enter") == 0)
		return (7);
	else if (ft_strcmp(str, "/connect") == 0)
		return (8);
	else if (ft_strcmp(str, "/invite") == 0)
		return (9);
	else if (ft_strcmp(str, "/delete") == 0)
		return (10);
	return (0);
}

int		is_line(t_buffer *buff)
{
	t_circular	*tmp;

	tmp = buff->front;
	while (tmp->link != buff->front)
	{
		if (ft_strchr(tmp->buffer, '\n'))
			return (1);
		tmp = tmp->link;
	}
	return (0);
}

void	free_node(t_listnode *node)
{
	t_buffer	*buff;

	buff = node->buff;
	while (buff)
		remove_node(buff);
	free(node);
	node = NULL;
}

char	*retrieve_command(int fd, int *curr_chan)
{
	t_listnode	*node;
	t_listnode	**tmp;
	char		*line;

	line = NULL;
	tmp = &g_info.graph->list[0].users;
	node = *tmp;
	while (node != NULL)
	{
		if (node->fd == fd)
			break ;
		node = node->next;
	}
	if (node && node->line_count > 0)
	{
		line = get_line(node->buff);
		if (line)
		{
			remove_line(&node->buff);
			node->line_count--;
			*curr_chan = node->curr_chan;
		}
	}
	return (line);
}

char	*copy_until_char(char *str, char c)
{
	int		x;
	int		len;
	char	*new;

	x = 0;
	new = NULL;
	len = ft_strlen(str);
	new = ft_strnew(len);
	if (new)
	{
		while (str[x] != c && c != '\0')
		{
			new[x] = str[x];
			x++;
		}
	}
	return (new);
}
