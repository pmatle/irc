# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/21 14:31:33 by pmatle            #+#    #+#              #
#*   Updated: 2018/10/23 21:50:45 by mafernan         ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

NAME1 = server

NAME2 = client

SRCS = utils.c handle_command.c file_handling.c file_parsing.c error.c \
	   handle_client.c network_utils.c utils1.c utils2.c commands.c utils4.c \
	   select.c init_graph.c circular_buffer.c handle_reading.c utils3.c \
	   channel.c channel_utils.c utils5.c free_mem.c message.c commands1.c utils6.c \
	   client_utils.c servdata.c

OBJS = utils.o handle_command.o file_handling.o file_parsing.o error.o \
	   handle_client.o network_utils.o utils1.o utils2.o commands.o utils4.o \
	   select.o init_graph.o circular_buffer.o handle_reading.o utils3.o \
	   channel.o channel_utils.o utils5.o free_mem.o message.o commands1.o utils6.o \
	   client_utils.o servdata.o

SRCS1 = server.c

SRCS2 = client.c

OBJS1 = server.o

OBJS2 = client.o

CC = gcc

CFALGS = -Wall -Wextra -Werror

LIBFT_HEADER = libft/includes/

LIBFT = -L libft/ -lft

all: $(NAME1) $(NAME2)

$(NAME1):
	make -C libft/
	$(CC) $(CFLAGS) -c $(SRCS) $(SRCS1) -I $(LIBFT_HEADER)
	$(CC) $(CFLAGS) $(OBJS) $(OBJS1) $(LIBFT)  -o $(NAME1)

$(NAME2):
	$(CC) $(CFLAGS) -c $(SRCS2) -I $(LIBFT_HEADER)
	$(CC) $(CFLAGS) $(OBJS) $(OBJS2) $(LIBFT)  -o $(NAME2)

clean:
	make clean -C libft/
	/bin/rm -rf $(OBJS) $(OBJS1) $(OBJS2)

fclean: clean
	make fclean -C libft
	/bin/rm -rf $(NAME1) $(NAME2)

re: fclean all

