/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 10:53:15 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:15:53 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	check_and_delete(t_buffer **buff)
{
	int		len;
	char	*str;

	str = ft_strchr((*buff)->front->buffer, '\n');
	len = ft_strlen(str);
	if (len > 1)
	{
		ft_memcpy((*buff)->front->buffer, (str + 1), ft_strlen(str + 1));
		(*buff)->front->buffer[len] = '\0';
	}
	else
		remove_node((*buff));
}

int		accept_connections(int fd, int max)
{
	int						new_fd;
	socklen_t				addrlen;
	struct sockaddr_storage	client;

	addrlen = sizeof(client);
	new_fd = accept(fd, (struct sockaddr *)&client, &addrlen);
	if (new_fd == -1)
		ft_putendl("Error: Failed to add new client.");
	else
	{
		add_client(new_fd);
		FD_SET(new_fd, &g_info.master);
		if (new_fd > max)
			max = new_fd;
		display_client(client);
	}
	return (max);
}

void	handle_not_connected(void)
{
	char	line[255];
	int		ret;

	while (1)
	{
		ret = read(0, line, 255);
		if (ret <= 0)
			continue;
		if (ft_strcmp(line, "quit") == 0)
			break ;
		if (ft_strncmp(line, "/connect", 8) == 0)
			connect_client(line);
		else if (is_command(line))
			ft_putendl("Error: Client not connect to a server");
		else
			ft_putendl("Error: Command not found");
		ft_bzero(line, 255);
	}
}

void	connect_client(char *str)
{
	char	**arr;

	arr = ft_strsplit(str, ' ');
	if (!arr)
	{
		ft_putendl("Failed to connect to server");
		return ;
	}
	if (arr_len(arr) != 3)
	{
		ft_putendl("/connect <server> [port]");
		return ;
	}
	start_client(arr[1], arr[2]);
	free_2d(arr);
}

void	start_client(char *address, char *str)
{
	int				err;
	int				fd;
	int				num;
	char			*port;
	struct addrinfo	*info;

	num = ft_atoi(str);
	port = ft_itoa(num);
	if (!port)
		return ;
	info = init(address, port, "client");
	fd = connect_to_server(info);
	if (fd == -1)
		handle_error(err, "client");
	ft_putendl("Client connecting...");
	handle_prompt(fd);
}
