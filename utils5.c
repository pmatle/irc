/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils5.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 08:42:01 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:16:00 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int			ft_arr_strlen(char **arr)
{
	int			x;
	int			max;
	int			len;

	x = 0;
	len = 0;
	max = arr_len(arr);
	while (x < max)
	{
		len += ft_strlen(arr[x]);
		x++;
	}
	return (len);
}

int			user_exists(int index, int fd)
{
	t_listnode	*node;

	node = g_info.graph->list[index].users;
	while (node != NULL)
	{
		if (node->fd == fd)
			return (1);
		node = node->next;
	}
	return (0);
}

t_buffer	*init_buffer(void)
{
	t_buffer	*buff;

	buff = ft_memalloc(sizeof(*buff));
	if (!buff)
		return (NULL);
	buff->rear = NULL;
	buff->front = NULL;
	return (buff);
}

char		*append_user(char *list, char *name)
{
	int		len;
	char	*tmp;

	len = ft_strlen(list) + ft_strlen(name) + 1;
	tmp = ft_strnew(len);
	if (!tmp)
		return (NULL);
	tmp = ft_memcpy(tmp, list, ft_strlen(list));
	tmp = ft_strncat(tmp, name, ft_strlen(name));
	tmp = ft_strncat(tmp, "\n", 1);
	free(list);
	list = tmp;
	return (list);
}

char		*retrieve_message(int fd, int channel)
{
	t_listnode	*node;
	t_listnode	**tmp;
	char		*temp;
	char		*message;

	message = NULL;
	tmp = &g_info.graph->list[channel].users;
	node = *tmp;
	while (node != NULL)
	{
		if (node->fd == fd)
			break ;
		node = node->next;
	}
	if (node && node->message_count > 0)
	{
		temp = get_line(node->messages);
		if (temp)
		{
			remove_line(&node->messages);
			node->message_count--;
			message = ft_strjoin(temp, "\n");
		}
	}
	return (message);
}
