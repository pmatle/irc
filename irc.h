/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   irc.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 08:54:55 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 21:50:15 by mafernan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IRC_H
# define IRC_H

# include <stdio.h>
# include <sys/select.h>
# include <sys/types.h>
# include <arpa/inet.h>
# include <sys/socket.h>
# include <netdb.h>
# include <signal.h>
# include <sys/mman.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <sys/types.h>
# include "libft/includes/libft.h"

typedef struct	s_circular
{
	char				buffer[255];
	int					end;
	struct s_circular	*link;
}				t_circular;

typedef struct	s_buffer
{
	t_circular			*rear;
	t_circular			*front;
}				t_buffer;

typedef struct	s_listnode
{
	char				name[9];
	int					fd;
	int					curr_chan;
	int					line_count;
	int					message_count;
	t_buffer			*buff;
	t_buffer			*messages;
	struct s_listnode	*next;
}				t_listnode;

typedef struct	s_list
{
	char				channel_name[55];
	int					free;
	int					max;
	fd_set				channel_set;
	t_listnode			*users;
}				t_list;

typedef struct	s_graph
{
	int					vertexes;
	t_list				*list;
}				t_graph;

typedef struct	s_serverinfo
{
	t_graph				*graph;
	fd_set				master;
	fd_set				read_fds;
	fd_set				write_fds;
	fd_set				except_fds;
}				t_serverinfo;

typedef struct	s_file
{
	void		*data;
	uint64_t	size;
}				t_file;

typedef struct	s_servdata
{
	char				*data;
	struct s_servdata	*next;
}				t_servdata;

typedef struct	s_clientinfo
{
	int			read_stdin_lines;
	int			count;
	t_servdata	*head;
	t_buffer	*read_stdin;
}				t_clientinfo;

t_serverinfo	g_info;
t_clientinfo	g_client;

int				join_channel(t_listnode *node, int index);
int				add_user(char *channel, t_listnode *node);
int				init_channels(void);
int				find_channel_slot(void);
int				init_graph(int size);
t_listnode		*new_node(char *name, int fd);
t_listnode		*duplicate_node(t_list *node);
int				add_to_channel(char *channel, int fd);
int				is_valid_port(char *str);
struct addrinfo	*init(char *addr, char *port, char *type);
void			handle_server_cmds(int fd, char *str);
void			free_2d(char **arr);
int				is_command(char *str);
int				send_data(int fd, void *data, uint64_t size);
int				receive_file(int fd, char *filename);
int				open_tmpfile(void);
int				send_filesize(int fd, uint64_t size);
int				arr_len(char **arr);
void			handle_error(int ecode, char *type);
void			print_message(int fd, char *full);
int				handle_server_command(int fd, char **arr, char *str);
int				check_command(char *str);
int				get_file_ptr(char *str, t_file *file);
void			handle_ls(int fd, char **arr, int flag);
int				handle_reading(fd_set set, int listen, int max);
void			handle_writing(fd_set set, int max);
void			handle_execpt(fd_set set, int max);
int				add_client(int fd);
int				reset_fds(int flag);
t_buffer		*init_buffer(void);
int				select_loop(int fd);
int				is_line(t_buffer *buff);
int				add_data(t_listnode **client, char *str, int flag);
int				accept_connections(int fd, int max);
void			display_client(struct sockaddr_storage client);
char			*get_line(t_buffer *buff);
void			handle_nick(int fd, char **arr);
int				handle_commands(char *str, int fd, t_listnode *users, int chan);
int				remove_client(int fd);
int				remove_node(t_buffer *buff);
void			free_node(t_listnode *node);
char			*retrieve_command(int fd, int *curr_chan);
char			*copy_until_char(char *str, char c);
int				remove_line(t_buffer **buff);
void			check_and_delete(t_buffer **buffer);
void			connect_client(char *str);
void			handle_not_connected(void);
void			connect_client(char *str);
void			handle_prompt(int fd);
int				connect_to_server(struct addrinfo *info);
void			start_client(char *addr, char *port);
int				create_channel(t_listnode *node, char *channel);
char			*get_members(int index, int fd);
t_listnode		*get_client(int fd);
int				channel_exists(char *channel);
int				user_exists(int index, int fd);
void			free_circular(t_buffer *buff);
void			free_listnode(t_listnode *node);
void			handle_join(int fd, int index, char **arr);
void			handle_list(int fd, int channel);
void			handle_create(int fd, int channel, char **arr);
char			*get_channels(int fd, int channel);
char			*append_user(char *list, char *name);
void			broadcast_message(int fd, int channel, char **arr);
void			private_message(int fd, int channel, char **arr);
void			handle_msg(int fd, int channel, char **arr);
char			*retrieve_message(int fd, int channel);
int				ft_arr_strlen(char **arr);
void			change_channel(int fd, int index);
void			enter_channel(int fd, int channel);
void			handle_enter(int fd, char **arr);
int				do_select(fd_set *read_fds, fd_set *write_fds, int max);
void			get_set(int fd, fd_set *write, fd_set *read);
int				get_max(fd_set *set, int max);
void			read_from_input(fd_set set, int fd);
void			write_to_output(fd_set set, int fd);
char			*get_data(int fd, int *val);
void			accept_data(int fd, int flag);
void			send_to_output(int fd, int flag);
void			write_to_stdin(int fd);
void			init_client(void);
void			add_servdata(t_servdata **data, char *str);
char			*get_servdata(t_servdata **head);
void			free_servdata(t_servdata *head);

#endif
