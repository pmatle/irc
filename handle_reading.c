/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_reading.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 04:56:00 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 21:51:57 by mafernan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

char			*get_data(int fd, int *val)
{
	char	*buff;

	*val = -1;
	buff = ft_strnew(255);
	if (buff)
		*val = recv(fd, buff, 255, 0);
	if (*val <= 0)
		return (NULL);
	return (buff);
}

int				read_data(int fd)
{
	t_graph		*graph;
	t_listnode	**node;
	t_listnode	*tmp;
	char		*data;
	int			x;

	data = get_data(fd, &x);
	if (x > 0)
	{
		node = &g_info.graph->list[0].users;
		tmp = *node;
		while (tmp != NULL)
		{
			if (tmp->fd == fd)
			{
				add_data(&tmp, data, 1);
				break ;
			}
			tmp = tmp->next;
		}
	}
	free(data);
	return ((x <= 0) ? -1 : 0);
}

void			run_command(int fd)
{
	char		*str;
	int			curr_chan;
	t_listnode	*members;

	str = retrieve_command(fd, &curr_chan);
	if (!str || ft_strlen(str) == 0)
		return ;
	members = g_info.graph->list[curr_chan].users;
	handle_commands(str, fd, members, curr_chan);
}

int				handle_reading(fd_set set, int listen, int max)
{
	int		fd;
	int		ret;

	fd = 0;
	while (fd <= max)
	{
		if (FD_ISSET(fd, &set))
		{
			if (fd == listen)
				max = accept_connections(fd, max);
			else
			{
				ret = read_data(fd);
				if (ret == -1)
				{
					remove_client(fd);
					ft_putendl("Client disconnected");
				}
				else
					run_command(fd);
			}
		}
		fd++;
	}
	return (max);
}
