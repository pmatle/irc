/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_client.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 11:33:01 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:15:25 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	download_file(int fd, char **arr, char *str)
{
	if (arr_len(arr) == 2)
	{
		send_data(fd, str, ft_strlen(str));
		send_data(fd, "\n", 1);
		receive_file(fd, arr[1]);
	}
	else
		ft_putendl("Error: Only able to get one file at a time");
}

void	put_command(int fd, char **arr, char *str)
{
	t_file		file;
	uint64_t	size;
	int			ret;

	if (arr_len(arr) == 2)
	{
		send_data(fd, str, ft_strlen(str));
		send_data(fd, "\n", 1);
		ret = get_file_ptr(arr[1], &file);
		if (ret == 0)
			ft_putendl("Failed to put file...try again");
		else
		{
			size = file.size;
			size = htonl(size);
			send_data(fd, &size, sizeof(size));
			send_data(fd, file.data, file.size);
		}
	}
	else
		ft_putendl("Error: Only able to put one file at a time");
}

int		handle_server_command(int fd, char **arr, char *str)
{
	int			ret;
	t_file		file;

	ret = check_command(arr[0]);
	if (ret == 0)
		return (-1);
	handle_server_cmds(fd, str);
	return (0);
}

void	read_from_input(fd_set set, int max)
{
	int		fd;

	fd = 0;
	while (fd <= max)
	{
		if (FD_ISSET(fd, &set))
		{
			if (fd == max)
				accept_data(fd, 0);
			else
				accept_data(fd, 1);
		}
		fd++;
	}
}

void	write_to_output(fd_set set, int max)
{
	int		fd;
	char	*line;

	fd = 0;
	while (fd <= max)
	{
		if (FD_ISSET(fd, &set))
		{
			if (fd == max)
			{
				send_to_output(fd, 2);
			}
			else
			{
				send_to_output(fd, 3);
			}
		}
		fd++;
	}
}
