/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/29 08:08:43 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 15:52:55 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		get_max(fd_set *set, int max)
{
	int		x;
	fd_set	tmp;
	int		new_max;

	x = 0;
	new_max = 0;
	FD_ZERO(&tmp);
	while (x <= max)
	{
		if (FD_ISSET(x, set))
		{
			FD_SET(x, &tmp);
			if (x > new_max)
				new_max = x;
		}
		x++;
	}
	*set = tmp;
	return (new_max);
}

int		do_select(fd_set *read_fds, fd_set *write_fds, int max)
{
	int			ret;

	ret = select(max + 1, read_fds, write_fds, NULL, NULL);
	return (ret);
}

int		select_loop(int fd)
{
	int				max;
	int				ret;
	char			*str;

	max = fd;
	init_graph(10);
	init_channels();
	while (1)
	{
		reset_fds(0);
		max = get_max(&g_info.master, max);
		g_info.read_fds = g_info.master;
		g_info.write_fds = g_info.master;
		ret = do_select(&g_info.read_fds, &g_info.write_fds, max);
		if (ret == -1)
		{
			ft_putendl("An error occured in server");
			return (-1);
		}
		max = handle_reading(g_info.read_fds, fd, max);
		max = get_max(&g_info.master, max);
		handle_writing(g_info.write_fds, max);
	}
	return (1);
}
