/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   circular_buffer.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 05:22:06 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 08:15:55 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int			add_data(t_listnode **user, char *str, int flag)
{
	t_buffer	*tmp;
	t_circular	*node;

	tmp = (flag == 3) ? (*user)->messages : (*user)->buff;
	if (ft_strlen(str) != 0)
	{
		node = (void *)ft_memalloc(sizeof(*node));
		if (node)
		{
			if (ft_strchr(str, '\n') && flag != 3)
				(*user)->line_count++;
			else if (ft_strchr(str, '\n') && flag == 3)
				(*user)->message_count++;
			ft_memcpy(node->buffer, str, ft_strlen(str));
			node->end = flag;
			if (tmp->front == NULL)
				tmp->front = node;
			else
				tmp->rear->link = node;
			tmp->rear = node;
			tmp->rear->link = tmp->front;
		}
	}
	return (0);
}

int			remove_node(t_buffer *buff)
{
	t_circular	*tmp;

	if (buff->front == NULL)
		return (0);
	if (buff->front == buff->rear)
	{
		free(buff->front);
		buff->front = NULL;
		buff->rear = NULL;
	}
	else
	{
		tmp = buff->front;
		buff->front = buff->front->link;
		buff->rear->link = buff->front;
		free(tmp);
	}
	return (1);
}

char		*get_command(char *line, char *str)
{
	char	*s1;
	char	*s2;

	s1 = copy_until_char(str, '\n');
	s2 = ft_strjoin(line, s1);
	free(line);
	free(s1);
	line = s2;
	return (line);
}

char		*get_line(t_buffer *buff)
{
	t_circular	*tmp;
	char		*s1;
	char		*s2;
	char		*line;

	if (buff != NULL)
	{
		tmp = buff->front;
		line = ft_strnew(1);
		if (!tmp || !line)
			return (NULL);
		while (tmp->link != buff->front)
		{
			if (ft_strchr(tmp->buffer, '\n'))
				return (get_command(line, tmp->buffer));
			s1 = ft_strjoin(line, tmp->buffer);
			free(line);
			line = s1;
			tmp = tmp->link;
		}
		if (tmp && ft_strchr(tmp->buffer, '\n'))
			return (get_command(line, tmp->buffer));
	}
	return (NULL);
}

int			remove_line(t_buffer **buff)
{
	if (buff != NULL)
	{
		if (!(*buff)->front)
			return (0);
		if (ft_strchr((*buff)->front->buffer, '\n'))
		{
			check_and_delete(buff);
			return (1);
		}
		while ((*buff)->front->link != (*buff)->front)
		{
			if (ft_strchr((*buff)->front->buffer, '\n'))
			{
				check_and_delete(buff);
				return (1);
			}
			remove_node((*buff));
		}
		if (ft_strchr((*buff)->front->buffer, '\n'))
		{
			check_and_delete(buff);
			return (1);
		}
	}
	return (0);
}
