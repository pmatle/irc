/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   channel_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/28 10:01:28 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 09:52:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	change_channel(int fd, int index)
{
	t_listnode	*user;

	user = g_info.graph->list[0].users;
	while (user != NULL)
	{
		if (user->fd == fd)
		{
			user->curr_chan = index;
			break ;
		}
		user = user->next;
	}
}

int		join_channel(t_listnode *node, int index)
{
	if (user_exists(index, node->fd) == 0)
	{
		node->next = g_info.graph->list[index].users;
		g_info.graph->list[index].users = node;
		FD_SET(node->fd, &g_info.graph->list[index].channel_set);
		change_channel(node->fd, index);
	}
	else
	{
		free_circular(node->buff);
		free_circular(node->messages);
		free(node);
		send_data(node->fd, "You are already a member of this channel\n", 41);
	}
	return (0);
}

int		add_user(char *channel, t_listnode *node)
{
	int		x;
	t_list	list;

	x = 0;
	while (x < g_info.graph->vertexes)
	{
		list = g_info.graph->list[x];
		if (ft_strcmp(channel, list.channel_name) == 0)
			return (join_channel(node, x));
		x++;
	}
	return (-1);
}

int		init_channels(void)
{
	t_list	*list;
	int		x;

	x = 0;
	while (x < g_info.graph->vertexes)
	{
		list = g_info.graph->list;
		if (x == 0)
		{
			list[x].free = 0;
			ft_memcpy(list[x].channel_name, "General", 7);
		}
		else
		{
			list[x].free = 1;
			ft_bzero(list[x].channel_name, ft_strlen(list[x].channel_name));
		}
		FD_ZERO(&list[x].channel_set);
		x++;
	}
	return (0);
}

int		find_channel_slot(void)
{
	t_graph		*graph;
	int			index;

	index = 0;
	graph = g_info.graph;
	while (index < graph->vertexes)
	{
		if (graph->list[index].free)
			return (index);
		index++;
	}
	return (-1);
}
