/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_graph.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 09:53:17 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 07:16:52 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int				init_graph(int size)
{
	int		x;

	g_info.graph = (void *)ft_memalloc(sizeof(*g_info.graph));
	if (g_info.graph)
	{
		g_info.graph->vertexes = size;
		g_info.graph->list = (void *)ft_memalloc(sizeof(*g_info.graph->list) *
				size);
		if (!g_info.graph->list)
		{
			free(g_info.graph);
			return (-1);
		}
		x = 0;
		while (x < size)
		{
			g_info.graph->list[x].users = NULL;
			FD_ZERO(&g_info.graph->list[x].channel_set);
			x++;
		}
		return (0);
	}
	return (-1);
}

t_listnode		*new_node(char *name, int fd)
{
	t_listnode		*node;

	node = (void *)ft_memalloc(sizeof(*node));
	if (!node)
		return (NULL);
	ft_memcpy(node->name, name, ft_strlen(name));
	node->buff = init_buffer();
	node->messages = init_buffer();
	node->fd = fd;
	node->curr_chan = 0;
	node->next = NULL;
	FD_SET(fd, &g_info.graph->list[0].channel_set);
	return (node);
}

int				add_client(int fd)
{
	static int	x = 0;
	char		*val;
	char		*name;
	t_listnode	*node;

	if (!(val = ft_itoa(x)))
		return (-1);
	name = ft_strjoin("no name", val);
	if (name)
	{
		node = new_node(name, fd);
		if (!node)
		{
			free(name);
			free(val);
			return (-1);
		}
		node->next = g_info.graph->list[0].users;
		g_info.graph->list[0].users = node;
		free(name);
		free(val);
		x++;
		return (0);
	}
	return (-1);
}

void			delete_node(int index, int fd)
{
	t_listnode		*node;
	t_listnode		*prev;

	node = g_info.graph->list[index].users;
	if (node != NULL && node->fd == fd)
	{
		g_info.graph->list[index].users = node->next;
		free_circular(node->buff);
		free_circular(node->messages);
		free(node);
		return ;
	}
	while (node != NULL && node->fd != fd)
	{
		prev = node;
		node = node->next;
	}
	if (node != NULL)
	{
		prev->next = node->next;
		free_circular(node->buff);
		free_circular(node->messages);
		free(node);
	}
}

int				remove_client(int fd)
{
	t_listnode		*node;
	t_graph			*graph;
	int				x;

	x = 0;
	graph = g_info.graph;
	while (x < graph->vertexes)
	{
		if (FD_ISSET(fd, &graph->list[x].channel_set))
		{
			FD_CLR(fd, &g_info.graph->list[x].channel_set);
			delete_node(x, fd);
		}
		x++;
	}
	FD_CLR(fd, &g_info.master);
	close(fd);
	return (0);
}
