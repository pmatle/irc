/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 07:12:15 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:16:34 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	add(t_buffer **head, char *data, int flag)
{
	t_circular	*node;
	t_buffer	*tmp;

	tmp = *head;
	if (ft_strlen(data) != 0)
	{
		node = (void *)ft_memalloc(sizeof(*node));
		if (node)
		{
			if (ft_strchr(data, '\n') && flag == 1)
				g_client.read_stdin_lines++;
			ft_memcpy(node->buffer, data, ft_strlen(data));
			if (tmp->front == NULL)
				tmp->front = node;
			else
				tmp->rear->link = node;
			tmp->rear = node;
			tmp->rear->link = tmp->front;
		}
	}
}

char	*extract_data(int fd, int flag, int *ret)
{
	char	*data;
	char	buff[255];

	if (flag == 0)
	{
		data = get_data(fd, ret);
	}
	else
	{
		*ret = read(fd, buff, 255);
		data = ft_strdup(buff);
	}
	return (data);
}

void	accept_data(int fd, int flag)
{
	int		ret;
	char	buff[255];
	char	*data;

	data = extract_data(fd, flag, &ret);
	if (ret > 0)
	{
		if (flag == 0)
			add_servdata(&g_client.head, data);
		else
			add(&g_client.read_stdin, data, flag);
		free(data);
	}
	else if (ret == 0 && flag == 0)
	{
		ft_putendl_fd("Server not found...exiting", 0);
		free(data);
		free_servdata(g_client.head);
		free_circular(g_client.read_stdin);
		exit(0);
	}
}

void	send_to_server(int fd, char *str)
{
	char	*message;

	message = ft_strjoin(str, "\n");
	if (!message)
	{
		free(str);
		return ;
	}
	remove_line(&g_client.read_stdin);
	g_client.read_stdin_lines--;
	send_data(fd, message, ft_strlen(message));
	free(message);
}

void	send_to_output(int fd, int flag)
{
	char		*str;

	if (flag == 3 && g_client.count > 0)
	{
		str = get_servdata(&g_client.head);
		if (!str)
			return ;
		ft_putstr(str);
		free(str);
	}
	else if (flag == 2 && g_client.read_stdin_lines > 0)
	{
		str = get_line(g_client.read_stdin);
		if (str)
			send_to_server(fd, str);
	}
}
