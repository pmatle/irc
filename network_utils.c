/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 15:19:58 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/19 07:35:31 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

uint64_t	get_file_size(int fd)
{
	int			ret;
	char		buff[8];
	uint64_t	file_size;

	ret = recv(fd, buff, sizeof(uint64_t), 0);
	if (ret < 0)
		return (0);
	file_size = *(uint64_t *)buff;
	file_size = ntohl(file_size);
	return (file_size);
}

void		*recieve_data(int fd, uint64_t size)
{
	char		*buff;
	uint64_t	remaining;
	uint64_t	received;
	int			ret;

	received = 0;
	remaining = size;
	buff = ft_memalloc(sizeof(*buff) * size);
	if (!buff)
		return (NULL);
	while (received < size)
	{
		ret = recv(fd, &buff[received], remaining, 0);
		if (ret == -1)
			return (NULL);
		received += ret;
		remaining -= ret;
	}
	return (buff);
}

int			create_file(char *filename, char *data, uint64_t size)
{
	int		fd;
	char	*file;
	int		err;

	if (ft_strchr(filename, '/'))
		filename = ft_strrchr(filename, '/') + 1;
	fd = open(filename, O_RDWR | O_CREAT | O_TRUNC, 0755);
	if (fd == -1)
		return (0);
	err = lseek(fd, size - 1, SEEK_SET);
	if (err == -1)
		return (0);
	ft_putchar_fd('\0', fd);
	file = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (file == MAP_FAILED)
		return (0);
	ft_memcpy(file, data, size);
	munmap(file, size);
	close(fd);
	return (1);
}

int			receive_file(int fd, char *filename)
{
	char			*data;
	int				success;
	uint64_t		file_size;

	file_size = get_file_size(fd);
	if (file_size)
	{
		data = recieve_data(fd, file_size);
		if (!data)
			return (0);
		success = create_file(filename, data, file_size);
		if (!success)
			return (0);
	}
	return (1);
}

int			send_data(int fd, void *data, uint64_t size)
{
	uint64_t	sent_bytes;
	uint64_t	remaining;
	int			ret;

	sent_bytes = 0;
	remaining = size;
	while (sent_bytes < size)
	{
		ret = send(fd, data + sent_bytes, remaining, 0);
		if (ret == -1)
			return (0);
		sent_bytes += ret;
		remaining -= ret;
	}
	return (1);
}
