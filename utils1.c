/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 17:24:28 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:16:17 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		open_tmpfile(void)
{
	int		fd;

	fd = open("/tmp/irc_tmp_data", O_RDWR | O_CREAT | O_APPEND | O_TRUNC,
			0755);
	return (fd);
}

int		send_filesize(int fd, uint64_t size)
{
	int		ret;

	size = htonl(size);
	ret = send(fd, &size, sizeof(uint64_t), 0);
	if (ret < 0)
	{
		ft_putendl("Failed to send data");
		return (-1);
	}
	return (0);
}

int		arr_len(char **arr)
{
	int		x;

	x = 0;
	while (arr[x])
	{
		x++;
	}
	return (x);
}

int		reset_fds(int flag)
{
	if (flag)
		FD_ZERO(&g_info.master);
	FD_ZERO(&g_info.read_fds);
	FD_ZERO(&g_info.write_fds);
	FD_ZERO(&g_info.except_fds);
	return (0);
}

void	display_client(struct sockaddr_storage client)
{
	struct in_addr		addr;
	struct sockaddr_in	*ipv4addr;
	char				*name;

	ipv4addr = (struct sockaddr_in *)&client;
	addr = ipv4addr->sin_addr;
	name = inet_ntoa(addr);
	ft_putstr("Client connected from : ");
	if (ft_strcmp(name, "0.0.0.0") == 0)
		ft_putendl("localhost");
	else
		ft_putendl(name);
}
