/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   servdata.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 09:40:36 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/23 21:53:44 by mafernan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void		add_servdata(t_servdata **head, char *data)
{
	t_servdata	*tmp;
	t_servdata	*node;

	node = (void *)ft_memalloc(sizeof(*node));
	if (!node)
		return ;
	node->data = ft_strdup(data);
	node->next = NULL;
	if (*head == NULL)
		*head = node;
	else
	{
		tmp = *head;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = node;
	}
	g_client.count++;
}

char		*get_servdata(t_servdata **head)
{
	t_servdata	*tmp;
	char		*data;

	if (*head == NULL)
		return (NULL);
	tmp = *head;
	(*head) = (*head)->next;
	data = tmp->data;
	free(tmp);
	g_client.count--;
	return (data);
}

void		free_servdata(t_servdata *head)
{
	t_servdata	*tmp;

	while (head != NULL)
	{
		tmp = head;
		head = head->next;
		free(tmp->data);
		free(tmp);
	}
}
