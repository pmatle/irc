/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/21 12:43:48 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/22 09:05:32 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int		bind_port(struct addrinfo *info)
{
	int		fd;
	int		flag;
	int		err;

	fd = -1;
	flag = 1;
	while (info != NULL)
	{
		fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
		if (fd == -1)
		{
			info = info->ai_next;
			continue ;
		}
		err = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
		if (err)
			handle_error(err, "server");
		err = bind(fd, info->ai_addr, info->ai_addrlen);
		if (!err)
			break ;
		close(fd);
		info = info->ai_next;
	}
	return (fd);
}

void	start_server(char *port)
{
	int					err;
	int					fd;
	struct addrinfo		*info;

	info = init(NULL, port, "server");
	fd = bind_port(info);
	if (fd == -1)
		handle_error(err, "server");
	err = listen(fd, 20);
	if (err)
		handle_error(err, "server");
	reset_fds(1);
	FD_SET(fd, &g_info.master);
	select_loop(fd);
}

int		main(int argc, char **argv)
{
	int		port;

	if (argc == 2)
	{
		if (is_valid_port(argv[1]))
			start_server(argv[1]);
		else
			ft_putendl_fd("Error: Invalid port number", 2);
	}
	else
		ft_putendl_fd("Usage: ./server <port>", 2);
	return (1);
}
