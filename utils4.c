/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/07 09:30:33 by pmatle            #+#    #+#             */
/*   Updated: 2018/10/24 10:15:36 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int			connect_to_server(struct addrinfo *info)
{
	int		fd;
	int		err;

	fd = -1;
	while (info != NULL)
	{
		fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
		if (fd == -1)
		{
			info = info->ai_next;
			continue;
		}
		err = connect(fd, info->ai_addr, info->ai_addrlen);
		if (!err)
			break ;
		info = info->ai_next;
	}
	return (fd);
}

void		handle_prompt(int fd)
{
	int			ret;
	char		*line;
	fd_set		read_set;
	fd_set		write_set;

	init_client();
	while (1)
	{
		get_set(fd, &write_set, &read_set);
		ret = do_select(&read_set, &write_set, fd);
		if (ret == -1)
		{
			ft_putendl("An error occured...exiting");
			return ;
		}
		read_from_input(read_set, fd);
		write_to_output(write_set, fd);
	}
}

t_listnode	*get_client(int fd)
{
	t_listnode	*node;

	node = g_info.graph->list[0].users;
	while (node != NULL)
	{
		if (node->fd == fd)
			return (node);
		node = node->next;
	}
	return (node);
}

int			channel_exists(char *channel)
{
	int			x;
	t_graph		*graph;

	x = 0;
	graph = g_info.graph;
	while (x < graph->vertexes)
	{
		if (ft_strcmp(graph->list[x].channel_name, channel) == 0)
			return (1);
		x++;
	}
	return (0);
}

void		handle_join(int fd, int channel_index, char **arr)
{
	int				err;
	t_listnode		*tmp;
	t_listnode		*node;

	if (arr_len(arr) == 2)
	{
		tmp = get_client(fd);
		if (!tmp)
			return ;
		node = new_node(tmp->name, fd);
		if (!node)
			return ;
		err = add_user(arr[1], node);
		if (err)
			send_data(fd, "Error: channel not found\n", 25);
	}
	else
	{
		send_data(fd, "Usage: /join [channel]\n", 23);
	}
}
