/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_handling.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/23 12:37:15 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/29 06:45:39 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void	send_file(int fd, t_file file, char *type)
{
	uint64_t		file_size;
	unsigned char	*buff;
	int				ret;

	if (ft_strcmp(type, "put") == 0)
		fd = 1;
	file_size = file.size;
	file_size = htonl(file_size);
	buff = (void *)file_size;
	ret = send_data(fd, &file_size, sizeof(uint64_t));
	if (ret < 0)
	{
		ft_putendl("Failed");
		send_data(fd, "Failed to ", 10);
		send_data(fd, type, ft_strlen(type));
		send_data(fd, " file...try again\n", 18);
	}
	ret = send_data(fd, file.data, file.size);
	if (ret < 0)
	{
		ft_putendl("Failed");
		send_data(fd, "Failed to ", 10);
		send_data(fd, type, ft_strlen(type));
		send_data(fd, " file...try again\n", 18);
	}
}

int		ft_send_file(int fd, char **arr, char *type)
{
	int		x;
	t_file	file;

	x = 1;
	while (arr[x])
	{
		if (get_file_ptr(arr[x], &file) == 0)
		{
			x++;
			continue ;
		}
		ft_putendl("Calling send file");
		send_file(fd, file, type);
		ft_putendl("After Calling send file");
		munmap(file.data, file.size);
		x++;
	}
	return (1);
}
