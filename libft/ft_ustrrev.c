/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/26 16:20:57 by pmatle            #+#    #+#             */
/*   Updated: 2018/09/08 08:53:16 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned char	*ft_ustrrev(unsigned char *str)
{
	int				x;
	int				y;
	unsigned char	c;

	x = 0;
	y = ft_ustrlen(str) - 1;
	while (y > x)
	{
		c = str[x];
		str[x] = str[y];
		str[y] = c;
		x++;
		y--;
	}
	return (str);
}
