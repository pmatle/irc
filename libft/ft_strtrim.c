/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/24 09:01:17 by pmatle            #+#    #+#             */
/*   Updated: 2017/10/22 13:33:04 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*str;
	int		x;
	int		len;
	int		y;

	if (s)
	{
		x = 0;
		y = 0;
		str = (char*)s;
		len = ft_strlen(str) - 1;
		while (str[x] == ' ' || str[x] == '\t' || str[x] == '\n')
			x++;
		while (str[len] == ' ' || str[len] == '\t' || str[len] == '\n')
		{
			len--;
			y++;
		}
		if (ft_strlen(&str[x]) == 0)
			return (ft_strnew(1));
		return (ft_strsub(str, x, ft_strlen(str) - (x + y + 1) + 1));
	}
	return (NULL);
}
